# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Oleg Titov <oleg.titov@gmail.com>
pkgname=svt-av1
pkgver=1.2.0
pkgrel=0
pkgdesc="Scalable Vector Technology for AV1 encoder (SVT-AV1 Encoder)"
url="https://01.org/svt"
arch="x86_64" # x86: inlining failed in call to always_inline '_mm_load_sd': target specific option mismatch
license="BSD-3-Clause-Clear"
options="!check" # No test suite from upstream
makedepends="cmake samurai yasm"
subpackages="$pkgname-dev $pkgname-libs"
source="https://gitlab.com/AOMediaCodec/SVT-AV1/-/archive/v$pkgver/SVT-AV1-v$pkgver.tar.bz2"
builddir="$srcdir/SVT-AV1-v$pkgver"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	export LDFLAGS="$LDFLAGS -Wl,-z,stack-size=1048576"
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=Release \
		$CMAKE_CROSSOPTS .
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
710b6e9082cb5397f436779e40b54da77aa97a882bb4c027acbd522d1ae6dd13191ff1307f835ff0d714f87d2b27f93ff2524c3d56f7ce0dee184f835ca7b424  SVT-AV1-v1.2.0.tar.bz2
"
